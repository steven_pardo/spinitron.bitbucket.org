# spinitron.bitbucket.org

A website using static HTML documents and CSS stylesheets to demonstrate the use of iframe
website integration with Spinitron.

This is live demo. Go to

* [spinitron.bitbucket.org](http://spinitron.bitbucket.org/)

to view the pages.

You could, for example, fork this repository and tinker with it to see how it works using Bitbucket
as web host, just as we do.